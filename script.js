//------------------------------------ Def Global Var ------------------------------------
{
    {
        var ctx ;
        var mapA = {};
        var map = {};
        var size = 20 ;
        var clicPress = false ;
        var loopInterval ;
        var firstPos = false;
    }
    {
        white = '#FFFFFF'
        gray  =	'#808080'
        black = '#000000'

        red = '#FF0000'
        lime = '#00FF00'
        blue = '#0000FF'

        maroon = '#800000'
        green = '#008000'
        navy = '#000080'

        yellow = '#FFFF00'
        aqua = '#00FFFF'
        cyan = '#00FFFF'
        Fuchsia = '#FF00FF'

        olive = '#808000'
        teal = '#008080'
        purple = '#800080'

        orange = '#FFA500'
        pink = '#FF1493'
    }
}

//------------------------------------ Help function ------------------------------------

function get_map(i,j){
    return mapA[i][j]
}

function draw(){
    let L = 800/size ;
    ctx.clearRect(0,0,800,800)
    for (let i = 0 ; i<size ; i++){
        for (let j = 0 ; j<size ; j++){
            
            ctx.fillStyle = map[i][j];
            ctx.fillRect(i* L , j * L, L-1, L-1);
            
        }
    }
}

function contacte(i,j,c,d=false){
    let s = 0; 
    if (get_map(i,j+1)== c){s++}
    if (get_map(i,j-1)== c){s++}
    if (get_map(i+1,j)== c){s++}
    if (get_map(i-1,j)== c){s++}

    if (get_map(i+1,j+1)== c && d){s++}
    if (get_map(i+1,j-1)== c && d){s++}
    if (get_map(i-1,j+1)== c && d){s++}
    if (get_map(i-1,j-1)== c && d){s++}
    return s;
}

function next(i,j){
    return [get_map(i+1,j),get_map(i-1,j),get_map(i,j+1),get_map(i,j-1)]
}

function txt(t=true){
    if (t){console.log('map')}
    else {console.log('mapA')}
    for (let i = 0 ; i<size ; i++){
        var l = '|';
        for (let j = 0 ; j<size ; j++){
            if (t){l+=map[i][j]}
            else {l+=mapA[i][j]}
            l+='|'
        }
        l+='|'
        console.log(l)
    }
}


//------------------------------------ Main function ------------------------------------

function start(event){
    ctx = document.getElementById('canvas').getContext('2d');
    ctx.clearRect(0,0,800,800)
    loopDraw()
    for (let i = -5 ; i<size+5 ; i++){
        let l = {};
        for (let j = -5 ; j<size+5 ; j++){
            //draw(i,j,white)
            l[j]=white;
        }
        map[i]=l
        
    }
    mapA = JSON.parse(JSON.stringify(map))
    
    draw()
}

function actu(){
    var mode = document.getElementById('mode').value[0];
    for (let i = 0 ; i<size ; i++){
        for (let j = 0 ; j<size ; j++){

                 if (mode == 0){ my_project(i,j)}
            else if (mode == 1){ game_of_life(i,j)}
            else if (mode == 2){ langton(i,j)}
        }
    }
    console.log('Actu',mapA==map)
    draw()
    mapA = JSON.parse(JSON.stringify(map))
}

//------------------------------------ Rule's function ------------------------------------

function game_of_life(i,j){

    if (contacte(i,j,black,true)==3 && get_map(i,j)==white){map[i][j]=black}

    else if (contacte(i,j,black,true)>3 && get_map(i,j)==black){map[i][j]=white}
    else if (contacte(i,j,black,true)<2 && get_map(i,j)==black){map[i][j]=white}
}

function my_project(i,j){

    if (get_map(i,j)==yellow){ var h ; }
    else if (get_map(i,j)==orange){map[i][j]=red}
    else if (contacte(i,j,orange)>=1 && get_map(i,j)==blue){map[i][j]=orange}
    else if (contacte(i,j,orange)==0 && get_map(i,j)==red){map[i][j]=blue}
    
    else if (contacte(i,j,white)!=0 && get_map(i,j)==lime){
        if (get_map(i+1,j)==white){map[i][j]=blue ; map[i+1][j]=blue}
        else if (get_map(i-1,j)==white){map[i][j]=blue ; map[i-1][j]=blue}
        else if (get_map(i,j+1)==white){map[i][j]=blue ; map[i][j+1]=blue}
        else if (get_map(i,j-1)==white){map[i][j]=blue ; map[i][j-1]=blue}
    }
    else if (contacte(i,j,red)!=0 && get_map(i,j)==lime){
        map[i][j]=blue
        if (get_map(i+1,j)==red){map[i+1][j]=lime}
        if (get_map(i-1,j)==red){map[i-1][j]=lime}
        if (get_map(i,j+1)==red){map[i][j+1]=lime}
        if (get_map(i,j-1)==red){map[i][j-1]=lime}
    }
    
    else if (contacte(i,j,green)>=1 && get_map(i,j)==blue){map[i][j]=green}

    else if (contacte(i,j,yellow)>=2 && get_map(i,j)==white){map[i][j]=blue}
    else if (contacte(i,j,yellow)+contacte(i,j,blue)>=3 && get_map(i,j)==white){map[i][j]=blue}

}

function langton(i,j){
    // https://www.diga.me.uk/LangtonLoops.html

    function color(num){
    if( num == 0){return white}
    else if( num == 1){return blue}
    else if( num == 2){return red} 
    else if( num == 3){return lime} 
    else if( num == 4){return yellow} 
    else if( num == 5){return pink} 
    else if( num == 6){return black} 
    else if( num == 7){return cyan} 
    
    }

    var rules = [ 
    '000012',
    '000020',
    '000030',
    '000050',
    '000063',
    '000071',
    '000112',
    '000122',
    '000132',
    '000212',
    '000220',
    '000230',
    '000262',
    '000272',
    '000320',
    '000525',
    '000622',
    '000722',
    '001022',
    '001120',
    '002020',
    '002030',
    '002050',
    '002125',
    '002220',
    '002322',
    '005222',
    '012321',
    '012421',
    '012525',
    '012621',
    '012721',
    '012751',
    '014221',
    '014321',
    '014421',
    '014721',
    '016251',
    '017221',
    '017255',
    '017521',
    '017621',
    '017721',
    '025271',
    '100011',
    '100061',
    '100077',
    '100111',
    '100121',
    '100211',
    '100244',
    '100277',
    '100511',
    '101011',
    '101111',
    '101244',
    '101277',
    '102026',
    '102121',
    '102211',
    '102244',
    '102263',
    '102277',
    '102327',
    '102424',
    '102626',
    '102644',
    '102677',
    '102710',
    '102727',
    '105427',
    '111121',
    '111221',
    '111244',
    '111251',
    '111261',
    '111277',
    '111522',
    '112121',
    '112221',
    '112244',
    '112251',
    '112277',
    '112321',
    '112424',
    '112621',
    '112727',
    '113221',
    '122244',
    '122277',
    '122434',
    '122547',
    '123244',
    '123277',
    '124255',
    '124267',
    '125275',
    '200012',
    '200022',
    '200042',
    '200071',
    '200122',
    '200152',
    '200212',
    '200222',
    '200232',
    '200242',
    '200250',
    '200262',
    '200272',
    '200326',
    '200423',
    '200517',
    '200522',
    '200575',
    '200722',
    '201022',
    '201122',
    '201222',
    '201422',
    '201722',
    '202022',
    '202032',
    '202052',
    '202073',
    '202122',
    '202152',
    '202212',
    '202222',
    '202272',
    '202321',
    '202422',
    '202452',
    '202520',
    '202552',
    '202622',
    '202722',
    '203122',
    '203216',
    '203226',
    '203422',
    '204222',
    '205122',
    '205212',
    '205222',
    '205521',
    '205725',
    '206222',
    '206722',
    '207122',
    '207222',
    '207422',
    '207722',
    '211222',
    '211261',
    '212222',
    '212242',
    '212262',
    '212272',
    '214222',
    '215222',
    '216222',
    '217222',
    '222272',
    '222442',
    '222462',
    '222762',
    '222772',
    '300013',
    '300022',
    '300041',
    '300076',
    '300123',
    '300421',
    '300622',
    '301021',
    '301220',
    '302511',
    '401120',
    '401220',
    '401250',
    '402120',
    '402221',
    '402326',
    '402520',
    '403221',
    '500022',
    '500215',
    '500225',
    '500232',
    '500272',
    '500520',
    '502022',
    '502122',
    '502152',
    '502220',
    '502244',
    '502722',
    '512122',
    '512220',
    '512422',
    '512722',
    '600011',
    '600021',
    '602120',
    '612125',
    '612131',
    '612225',
    '700077',
    '701120',
    '701220',
    '701250',
    '702120',
    '702221',
    '702251',
    '702321',
    '702525',
    '702720'
    ]

    for (let k = 0 ; k<rules.length ; k++){
        for (let n = 0 ; n < 4 ; n++){
            if (color(rules[k][0])==get_map(i,j)){
                if (
                    get_map(i+1,j)==color(rules[k][1+(n+0)%4]) && 
                    get_map(i,j+1)==color(rules[k][1+(n+1)%4]) && 
                    get_map(i-1,j)==color(rules[k][1+(n+2)%4]) &&
                    get_map(i,j-1)==color(rules[k][1+(n+3)%4])
                ){ 
                    map[i][j]=color(rules[k][5])
                    //console.log('k=',k,'n:',n,'r:',rules[k][5],'c:',color(rules[k][5]),'m:',map[i][j])
                }
                //else{ console.log('Only good color')}
            }
            //else {console.log('all bad',k,rules[k][0],get_map(i,j))}
        }
    }

    
}

function Langton_Setup(){
    function color(num){
        if( num == 0){return white}
        else if( num == 1){return blue}
        else if( num == 2){return red} 
        else if( num == 3){return lime} 
        else if( num == 4){return yellow} 
        else if( num == 5){return pink} 
        else if( num == 6){return black} 
        else if( num == 7){return cyan} 
    }
    size = 40
    start()
    l = [" 22222222",
    "2170140142",
    "2022222202",
    "272    212",
    "212    212",
    "202    212",
    "272    212",
    "21222222122222",
    "20710710711111",
    " 2222222222222"]
    for (let i=0; i < l.length ; i++){
        for (let j=0; j<l[i].length ; j++){
            if (l[i][j] != ' ') {
                map[10+j][10+i]=color(l[i][j])
            }
        }
    }
    draw()
}

//------------------------------------ Interaction ------------------------------------

function clic(event){
    
    const x = Math.abs(Math.round( (event.x + window.pageXOffset)/(800/size)-0.5));
    const y = Math.abs(Math.round( (event.y + window.pageYOffset)/(800/size)-0.5));

    var input = document.getElementById('color').value ;
    var inpSquare = document.getElementById('square').checked ;
    var rempl = document.getElementById('rempl').checked ;
    
    if (input != '----' && inpSquare==false){
        map[x][y]= '#'+input.split('#')[1].substr(0,6)
        //console.log('#'+input.split('#')[1].substr(0,6))
    }
    else if(input != '----' && inpSquare==true){
        if (firstPos == false){ firstPos = [x,y]}
        else {
            const x1 = firstPos[0]
            const y1 = firstPos[1]
            for (let i = Math.min(x,x1); i<Math.max(x,x1)+1; i++){
                for (let j = Math.min(y,y1); j<Math.max(y,y1)+1; j++){
                    if (rempl){map[i][j]=String(input.split("'")[1])}
                    else if (i == x || i==x1 ||j==y || j==y1)
                    {map[i][j]=String(input.split("'")[1])}
                }
            }
            firstPos = false
        }
    }
    else {console.log(Error)}

    //console.log('Click on',x,y,'|',input.split("'")[1]);
    draw()
}

function loopDraw(){
    var input = document.getElementById('loop').checked;
    var speed = document.getElementById('rspeed').value;
    document.getElementById('speed').innerHTML = speed ; 

    clearInterval(loopInterval);
    if (input){
        //console.log('loopDraw T');
        loopInterval = setInterval(function(){actu()},speed*100);
    }
    else {
        //console.log('loopDraw F')
        clearInterval(loopInterval);
    }
    //console.log('loopDraw',speed*100,'ms  ',input)
}

function mouse(event){
    const x = Math.abs(Math.round(event.x/(800/size)-0.5));
    const y = Math.abs(Math.round(event.y/(800/size)-0.5));

    document.getElementById("donne" ).innerHTML = ''+x+' '+ y;
    document.getElementById("donne2").innerHTML = ''+event.x+' '+event.y;
    document.getElementById("donne3").innerHTML = ''+clicPress ;
    document.getElementById("donne4").innerHTML = 'Square pos1 : '+firstPos ;
}

function key(event){
    //console.log('key : ',event.key)

    var input = document.getElementById('loop').checked;
    if (event.key == ' ' || event.key == 'Enter'){
        if (input){ document.getElementById('loop').checked=false }
        else { document.getElementById('loop').checked = true}
        
        loopDraw()
    }
    else if (event.key == 'Escape'){
        for (let i = 0 ; i<size ; i++){
            for (let j = 0 ; j<size ; j++){
                map[i][j]=white
                mapA[i][j]=white
            }
        }
    }
}


//------------------------------------ Comment ------------------------------------

/*

Structure :

planeur :
xx              xx
x x            xx
x                x


*/
